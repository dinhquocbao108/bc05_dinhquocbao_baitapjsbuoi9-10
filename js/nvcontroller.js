// lấy dữ liệu từ form
function layDuLieuTuForm(){
        var taiKhoanNv = document.getElementById("tknv").value;
        var hoTenNv = document.getElementById("name").value;
        var emailNv = document.getElementById("email").value;
        var matKhauNv = document.getElementById("password").value;
        var ngayLam = document.getElementById("datepicker").value;
        var luongCoBan = document.getElementById("luongCB").value;
        var chucVu = document.getElementById("chucvu").value;
        var gioLam = document.getElementById("gioLam").value;
        // tạo thông tin nhân viên 
        var nv = new nhanVien(taiKhoanNv,hoTenNv,emailNv,matKhauNv,ngayLam,luongCoBan,chucVu,gioLam);
        return nv;
}
// render danh sách nhân viên 
function renderdsnv(nvArr){
    var contentHTML = "";
    for(var index=0;index<nvArr.length;index++){
        var currentNv = nvArr[index];
        var contentTr =  `
        <tr>
        <td>${currentNv.taiKhoan}</td>
        <td>${currentNv.hoTen}</td>
        <td>${currentNv.email}</td>
        <td>${currentNv.ngayLam}</td>
        <td>${currentNv.chucVu}</td>
        <td>${currentNv.tongLuong()}</td>
        <td>${currentNv.xepLoai()}</td>
        <td>
        <button onclick="xoaNv('${currentNv.taiKhoan}')" class="btn btn-danger">Xóa</button>
        <button onclick="suaNv('${currentNv.taiKhoan}')" class="btn btn-warning" data-toggle="modal"
        data-target="#myModal">Sửa</button>
        </td>
        </tr>
        `;  
        contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
// tìm vị trí nhân viên thông qua tài khoản
function timKiemViTri(id,nvArr){
    for(var index=0;index<nvArr.length;index++){
        var item = nvArr[index];
        if(item.taiKhoan==id){
            return index;
        }
    }
    return -1;
}
function showThongTinLenForm(nv){
    document.getElementById("tknv").value=nv.taiKhoan;
    document.getElementById("name").value=nv.hoTen;
    document.getElementById("email").value=nv.email;
    document.getElementById("password").value=nv.matKhau;
    document.getElementById("datepicker").value=nv.ngayLam;
    document.getElementById("luongCB").value=nv.luongCoBan;
    document.getElementById("chucvu").value=nv.chucVu;
    document.getElementById("gioLam").value=nv.gioLam;

}
function showMessageErr(idErr,message){
    document.getElementById(idErr).style.display = "block";
    document.getElementById(idErr).innerHTML = message;
}
function sapXepNv(dsnv,loaiNv,arrEmpty){
    for(var index=0;index<dsnv.length;index++){
        if(dsnv[index].xepLoai() == loaiNv){
            arrEmpty.push(dsnv[index]);
        }
    }
    return arrEmpty;
}





