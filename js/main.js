var dsnv = [];
function luuLocalStogare(){
    // lưu vào local storage
    let jsonDsnv = JSON.stringify(dsnv);
    localStorage.setItem("JSONDSNV",jsonDsnv); 
};
// lấy dữ liệu từ localstorage lên khi load trang
var dataJson = localStorage.getItem("JSONDSNV"); 
if(dataJson!==null){
    var nvArr = JSON.parse(dataJson);   
    for(var index=0;index<nvArr.length;index++){
        var nv = new nhanVien(
            nvArr[index].taiKhoan,
            nvArr[index].hoTen,
            nvArr[index].email,
            nvArr[index].matKhau,
            nvArr[index].ngayLam,
            nvArr[index].luongCoBan,
            nvArr[index].chucVu,
            nvArr[index].gioLam,
        );
        dsnv.push(nv);
    }
    renderdsnv(dsnv);
};
function themNv(){
    var nv = layDuLieuTuForm();
    var isValid = true;
    // validate tài khoản
    isValid = 
    kiemTraRong(nv.taiKhoan,"tbTKNV","tài khoản không được để rỗng")&&
    kiemTraChieuDai(nv.taiKhoan,4,6,"tbTKNV","tài khoản phải từ 4-6 kí số")&&
    kiemTraTrung(nv,dsnv);
    // validate họ và tên 
    isValid = isValid&
    (kiemTraRong(nv.hoTen,"tbTen","Họ và tên không được để rỗng")&&
    kiemTraChu(nv.hoTen,"tbTen","họ tên phải là chữ"));
    // validate email
    isValid = isValid&
    (kiemTraRong(nv.email,"tbEmail","email không được để rỗng")&&
    kiemTraEmail(nv.email,"tbEmail","email không đúng định dạng"));
    // validate mật khẩu
    isValid = isValid&
    (kiemTraRong(nv.matKhau,"tbMatKhau","mật khẩu không được để trống")&&
    kiemTraMatKhau(nv.matKhau,"tbMatKhau","mật Khẩu phải từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"));
    // validate ngày 
    isValid = isValid&
    (kiemTraRong(nv.ngayLam,"tbNgay","ngày làm không được để trống")&&
    kiemTraNgay(nv.ngayLam,"tbNgay","ngày làm không đúng định dạng"));
    // validate lương cơ bản 
    isValid = isValid&
    (kiemTraRong(nv.luongCoBan,"tbLuongCB","lương cơ bản không được để trống")&&
    kiemTraSo(nv.luongCoBan,"tbLuongCB","lương cơ bản phải là số")&&
    kiemTraValueNumber(nv.luongCoBan*1,"tbLuongCB",1000000,20000000,"lương cơ bản phải từ 1tr-20tr"));
    // validate chức vụ
    isValid = isValid&
    kiemTraChucVu(nv.chucVu,"tbChucVu");
    // validate giờ làm
    isValid = isValid&
    (kiemTraRong(nv.gioLam,"tbGiolam","giờ làm không được để trống")&&
    kiemTraValueNumber(nv.gioLam,"tbGiolam",80,200,"giờ làm phải từ 80-200 giờ"));
    if(isValid){
      dsnv.push(nv);
    luuLocalStogare();
    renderdsnv(dsnv);
    resetForm();   
    }
};
function xoaNv(id){
    var viTri = timKiemViTri(id,dsnv);
    if(viTri!==-1){
        dsnv.splice(viTri,1);
        luuLocalStogare();
        renderdsnv(dsnv);
    }
};
function suaNv(id){
    var viTri = timKiemViTri(id,dsnv);
    if(viTri==-1) return;
    var data = dsnv[viTri];
    showThongTinLenForm(data);
    document.getElementById("tknv").disabled = true;
}
function capNhatNv(){
    data = layDuLieuTuForm();
    var viTri = timKiemViTri(data.taiKhoan ,dsnv);
    if(viTri==-1) return;
    var validate = true;
    // validate họ và tên 
    validate = validate&
    (kiemTraRong(data.hoTen,"tbTen","Họ và tên không được để rỗng")&&
    kiemTraChu(data.hoTen,"tbTen","họ tên phải là chữ"));
    // validate email
    validate = validate&
    (kiemTraRong(data.email,"tbEmail","email không được để rỗng")&&
    kiemTraEmail(data.email,"tbEmail","email không đúng định dạng"));
    // validate mật khẩu
    validate = validate&
    (kiemTraRong(data.matKhau,"tbMatKhau","mật khẩu không được để trống")&&
    kiemTraMatKhau(data.matKhau,"tbMatKhau","mật Khẩu phải từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"));
    // validate ngày 
    validate = validate&
    (kiemTraRong(data.ngayLam,"tbNgay","ngày làm không được để trống")&&
    kiemTraNgay(data.ngayLam,"tbNgay","ngày làm không đúng định dạng"));
    // validate lương cơ bản 
    validate = validate&
    (kiemTraRong(data.luongCoBan,"tbLuongCB","lương cơ bản không được để trống")&&
    kiemTraSo(data.luongCoBan,"tbLuongCB","lương cơ bản phải là số")&&
    kiemTraValueNumber(data.luongCoBan*1,"tbLuongCB",1000000,20000000,"lương cơ bản phải từ 1tr-20tr"));
    // validate chức vụ
    validate = validate&
    kiemTraChucVu(data.chucVu,"tbChucVu");
    // validate giờ làm
    validate = validate&
    (kiemTraRong(data.gioLam,"tbGiolam","giờ làm không được để trống")&&
    kiemTraValueNumber(data.gioLam,"tbGiolam",80,200,"giờ làm phải từ 80-200 giờ"));


    if(validate){
      dsnv[viTri] = data;
    renderdsnv(dsnv);
    luuLocalStogare();
    document.getElementById("tknv").disabled = false;
    resetForm();  
    }
}
function resetForm(){
    document.getElementById("form").reset();
}
function timLoaiNv(){
    var search =document.getElementById("SEARCH").value;
    var newArr = [];

    if(search==1){
        sapXepNv(dsnv,"Nhân viên xuất sắc",newArr);
        renderdsnv(newArr);
    }else if(search==2){
        sapXepNv(dsnv,"Nhân viên giỏi",newArr);
        renderdsnv(newArr);
    }else if(search==3){
        sapXepNv(dsnv,"Nhân viên khá",newArr);
        renderdsnv(newArr);
    }else if(search==4){
        sapXepNv(dsnv,"Nhân viên trung bình",newArr);
        renderdsnv(newArr);
    }
}

