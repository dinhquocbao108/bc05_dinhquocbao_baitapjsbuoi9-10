function kiemTraTrung(id,dsnv){
    let index = timKiemViTri(id.taiKhoan,dsnv);
    if(index!==-1){
        showMessageErr("tbTKNV","mã nhân viên đã tồn tại");
        return false;
    }else{
        showMessageErr("tbTKNV","");
        return true;
    }
}
function kiemTraRong(value,idErr,message){
    if(value.length==0){
        showMessageErr(idErr,message);
        return false;
    }else{
        showMessageErr(idErr,"");
        return true;
    }
}
function kiemTraChieuDai(input,min,max,idErr,message){
    if(input.length>=min&&input.length<=max){
        showMessageErr(idErr,"");
        return true;
    }else{
        showMessageErr(idErr,message);
        return false;
    }
}
function kiemTraChu(value,idErr,message){
    var reg =/[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễếệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u;
    let isText = reg.test(value);
    if(isText){
        showMessageErr(idErr,"");
        return true;
    }else{
        showMessageErr(idErr,message);
        return false;
    }
}
function kiemTraEmail(value,idErr,message){
    var reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isEmail = reg.test(value);
    if(isEmail){
        showMessageErr(idErr,"");
        return true;
    }else{
        showMessageErr(idErr,message);
        return false;
    };
}
function kiemTraMatKhau(value,idErr,message){
    var reg = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{6,10}$/;
    let isPassword = reg.test(value)
    if(isPassword){
        showMessageErr(idErr,"");
        return true;
    }else{
        showMessageErr(idErr,message);
        return false;
    }
}
function kiemTraNgay(value,idErr,message){
    var reg = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
    let isDate = reg.test(value);
    if(isDate){
        showMessageErr(idErr,"");
        return true;
    }else{
        showMessageErr(idErr,message);
        return false;
    }
}
function kiemTraSo(value,idErr,message){
    var reg = /^\d+$/;
    let isNumber = reg.test(value);
    if(isNumber){
        showMessageErr(idErr,"");
        return true;
    }else{
        showMessageErr(idErr,message);
        return false;
    }
}
function kiemTraValueNumber(value,idErr,min,max,message){
    if(value>=min&&value<=max){
        showMessageErr(idErr,"");
        return true;
    }else{
        showMessageErr(idErr,message);
        return false;
    }
}
function kiemTraChucVu(value,idErr){
    if(value=="Chọn chức vụ"){
        showMessageErr(idErr,"phải chọn chức vụ hợp lệ");
        return false;
    }else{
        showMessageErr(idErr,"");
        return true;
    }
}

