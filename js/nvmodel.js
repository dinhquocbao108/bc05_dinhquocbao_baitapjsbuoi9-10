function nhanVien(taiKhoan,hoTen,email,matKhau,ngayLam,luongCoBan,chucVu,gioLam){
    this.taiKhoan = taiKhoan;
    this.hoTen = hoTen;
    this.email = email;
    this.matKhau = matKhau;
    this.ngayLam = ngayLam;
    this.luongCoBan = luongCoBan;
    this.chucVu = chucVu;
    this.gioLam = gioLam;
    this.tongLuong = function(){
        var tongLuong = 0;
        if(this.chucVu=="Sếp"){
            tongLuong = this.luongCoBan*3;
        }else if(this.chucVu=="Trưởng phòng"){
            tongLuong = this.luongCoBan*2;
        }else if(this.chucVu=="Nhân viên"){
            tongLuong = this.luongCoBan*1;
        }
        return tongLuong;
    }
    this.xepLoai = function(){
        var loaiNv ="";
        if(this.gioLam*1>=192){
            loaiNv = "Nhân viên xuất sắc";
        }else if(this.gioLam*1>=176){
            loaiNv = "Nhân viên giỏi";
        }else if(this.gioLam*1>=160){
            loaiNv = "Nhân viên khá";
        }else{
            loaiNv = "Nhân viên trung bình";
        }
        return loaiNv;
    }
}